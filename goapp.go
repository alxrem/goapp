// Copyright 2018-2019 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package goapp

import (
	"context"
	log "github.com/sirupsen/logrus"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

type Starter interface {
	Start(ctx context.Context) (*sync.WaitGroup, error)
}

type Application struct {
	title   string
	wg      sync.WaitGroup
	mu      sync.Mutex
	ctx     context.Context
	cancel  context.CancelFunc
	started []*sync.WaitGroup
}

func NewApplication(title string) *Application {
	log.Debugf("Starting %s...", title)

	ctx, cancel := context.WithCancel(context.Background())
	return &Application{
		title:   title,
		ctx:     ctx,
		cancel:  cancel,
		started: make([]*sync.WaitGroup, 0),
	}
}

func (a *Application) Start(s Starter) error {
	a.mu.Lock()
	defer a.mu.Unlock()

	wg, err := s.Start(a.ctx)
	if err != nil {
		return err
	}
	a.started = append(a.started, wg)
	return nil
}

func (a *Application) Finish() {
	log.Debugf("Finishing %s...", a.title)
	a.cancel()

	a.mu.Lock()
	defer a.mu.Unlock()

	for _, wg := range a.started {
		wg.Wait()
	}
	log.Infof("Finished %s", a.title)
}

func (a *Application) WaitTermination() {
	term := make(chan os.Signal)
	signal.Notify(term, os.Interrupt, syscall.SIGTERM)
	select {
	case <-term:
		log.Warn("Receiving SIGTERM, exiting gracefully...")
	}
}
