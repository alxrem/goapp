// Copyright 2018-2019 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package goapp_test

import (
	"context"
	"fmt"
	"gitlab.com/alxrem/goapp"
	"os"
	"sync"
	"syscall"
	"testing"
	"time"
)

type testRunServer struct {
	wg sync.WaitGroup
	t *testing.T
}

func (ts *testRunServer) Start(ctx context.Context) (*sync.WaitGroup, error) {
	ts.wg.Add(1)

	go func(ctx context.Context) {
		defer ts.wg.Done()

		select {
		case <-ctx.Done():
			return
		}
	}(ctx)

	return &ts.wg, nil
}

func TestWaitTermination(t *testing.T) {
	app := goapp.NewApplication("test wait")

	ts := &testRunServer{
		t: t,
	}

	_ = app.Start(ts)

	stopped := make(chan int)
	go func(app *goapp.Application) {
		app.WaitTermination()
		close(stopped)
	}(app)

	p, err := os.FindProcess(os.Getpid())
	if err != nil {
		t.Fatal(err)
	}

	time.Sleep(100 * time.Millisecond)
	err = p.Signal(syscall.SIGTERM)
	if err != nil {
		t.Fatal(err)
	}

	select {
	case <-stopped:
		t.Log("Server stopped")
	case <-time.After(time.Second):
		t.Fatal("Server is not stopped in 1 second")
	}
}

type testStartServer struct {
	wg      sync.WaitGroup
	started chan int
}

func (ts *testStartServer) Start(ctx context.Context) (*sync.WaitGroup, error) {
	close(ts.started)
	return &ts.wg, nil
}

func TestStart(t *testing.T) {
	started := make(chan int)

	app := goapp.NewApplication("test start")

	ts := &testStartServer{
		started: started,
	}

	_ = app.Start(ts)

	select {
	case <-ts.started:
		t.Log("Server started")
	case <-time.After(time.Second):
		t.Fatal("Server is not started in 1 second")
	}

	app.Finish()
}

type testFailedStartServer struct {
}

func (ts *testFailedStartServer) Start(ctx context.Context) (*sync.WaitGroup, error) {
	return nil, fmt.Errorf("start failed")
}

func TestFailedStart(t *testing.T) {
	app := goapp.NewApplication("test failed start")

	ts := &testFailedStartServer{}

	if err := app.Start(ts); err != nil {
		t.Log("Start failed")
	} else {
		t.Fatal("Server is not failed")
	}

	app.Finish()
}

type testFinishServer struct {
	wg      sync.WaitGroup
	started chan int
	stopped chan int
}

func (ts *testFinishServer) Start(ctx context.Context) (*sync.WaitGroup, error) {
	ts.wg.Add(1)
	go func(ctx context.Context) {
		defer ts.wg.Done()

		close(ts.started)
		select {
		case <-ctx.Done():
			close(ts.stopped)
		}
	}(ctx)
	return &ts.wg, nil
}

func TestFinish(t *testing.T) {
	app := goapp.NewApplication("test finish")

	started := make(chan int)
	stopped := make(chan int)

	ts := &testFinishServer{
		started: started,
		stopped: stopped,
	}

	_ = app.Start(ts)

	<-started

	go app.Finish()

	select {
	case <-stopped:
		t.Log("Server finished")
	case <-time.After(time.Second):
		t.Fatal("Server is not finished in 1 second")
	}
}

func TestFinishWithoutStarters(t *testing.T) {
	app := goapp.NewApplication("test finish without starters")

	stopped := make(chan int)
	go func() {
		app.Finish()
		close(stopped)
	}()

	select {
	case <-stopped:
		t.Log("Server finished")
	case <-time.After(time.Second):
		t.Fatal("Server is not finished in 1 second")
	}
}
